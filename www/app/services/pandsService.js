define(['./http'], function(http){
	return {
		getList: function(){
			return http.get('/api/products')
			.then(function(r){
				return r.select(function(it){
					return {
						name: it.name,
						price: it.price,
						quantity: it.quantity? it.quantity: 0
					};
				});
			});
		}
	}
});