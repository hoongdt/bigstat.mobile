define({
	load: function (name, req, onload, config) {
		var info = localStorage.getItem('user-info');
		if(info)
			info = JSON.parse(info);
		if(info){
			onload(info);
			window.usercontext = info;
		}
		else
			onload(false);
    }
});