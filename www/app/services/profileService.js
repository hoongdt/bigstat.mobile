define(['./http', 'common/map','dummy/profileServiceDummy'], function(http, map, Dummy){
	var dummy = new Dummy();
	var Service = {
		_getRequest: null,
		getList: function(role, filter){
			var ret = null;
			if(!this._getRequest)
				this._getRequest = http.get('/api/users/');
			return this._getRequest
			.then(function(r){
				r.loop(function(it){
					it.role = map.mapRole(it.role);
				});

				return r.where(function(it){
					return it.role == role;
				}).select(function(it){
					return {
						status: [1,2,3].random(),
						bigz: it.bigZ,
						firstName: it.firstName,
						lastName: it.lastName,
						id: it.id
					}
				});
			})
			// client filter
			.then(function(r){
				if(!filter)
					return r;
				if(filter.text)
					r = r.where(function(it){
						return it.firstName.indexOf(filter.text)>=0
						|| it.lastName.indexOf(filter.text)>=0;
					});
				if(filter.criterias && filter.criterias.multiple)									
					r = filtering.filterMultiMatch(r, 'type', filter.criterias.multiple);
				if(filter.criterias && filter.criterias.single)
					r = filtering.filterTime(r, 'date', filter.criterias.single);

				return r;
			})
			;
		},
		getProfile: function(id){
			return http.get('/api/users/'+id)
			.then(function(r){
				r.role = map.mapRole(r.role);
				var url = null;
				if(r.role == 'prospector'){
					url = '/api/profils/getCallOperatorIndicatorGlobal/'+id					
				}
				else if(r.role == 'agence'){
					url = '/api/profils/getBranchIndicatorGlobal/'+id					
				}
				else if(r.role == 'director'){
					url = '/api/profils/getDirectorIndicatorGlobal/'+id					
				}
				else if(r.role == 'commercial'){
					url = '/api/profils/getSellerIndicatorGlobal/'+id					
				}
				else if(r.role == 'service-agent'){
					url = '/api/profils/getTechnicianIndicatorGlobal/'+id					
				}
				else{
					notSupportedException();
				}
				return http.get(url)
				.then(function(t1){
					return {
						userInfo: r,
						indicator: t1,
						category: r.role
					}
				});				
			});
		},
		getLastAppointments: function(id){
			return dummy.getLastAppointments();
		},
		getProductRank: function(id){
			return dummy.getProductRank();
		},
		getCaData: function(id){
			return dummy.getCaData();
		},
		getTauxData: function(id){
			return dummy.getTauxData();			
		}
	};

	return Service;
});