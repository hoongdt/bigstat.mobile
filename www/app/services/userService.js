define(['./http','common/map'], function(http,map){
	var Service = {
		_userInfo: null,
		login: function(email, password){	
			var that = this;		
			return http.post('/api/account/login',{
				username: email,
				password: password
			}).then(function(r){
				localStorage.setItem('token', r.accessToken);
				return that.authorized();
			});
		},
		authorized: function(){
			var that = this;		
			var ret = $.Deferred();
			if(!localStorage.getItem('token')){
				ret.resolve(false);
				return ret;
			}

			http.get('/api/account/userinfo').done(function(r){
				ret.resolve(true);
			})
			.done(function(r){
				that._userInfo = r;
				r.role = map.mapRole(r.roles[0]);
				localStorage.setItem('user-info', JSON.stringify(r));
			})
			.fail(function(){
				ret.resolve(false);
			});
			return ret;
		},
		logoff: function(){
			this._userInfo = null;
			localStorage.removeItem('user-info');
			localStorage.removeItem('token');
		}
	};

	return Service;
})