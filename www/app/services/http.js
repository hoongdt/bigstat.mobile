define(["app"],function(app){
	var	_baseUrl = 'http://develop.bigstat.ovh';

	var Service = {	
		get: function(url, req){
			app.ionicLoading.show();
			return $.ajax({
			  dataType: "json",
			  url: _baseUrl+url,
			  data: req,
			  headers:{
			  	Authorization: "Bearer " + localStorage.getItem('token')
			  }
			})
			.always(function(){
				app.ionicLoading.hide();	
			});
		},
		post: function(url, req){
			app.ionicLoading.show();
			return $.ajax({
			  type: "POST",
			  dataType: "json",
			  url: _baseUrl+url,
			  data: req,
			  headers:{
			  	Authorization: "Bearer " + localStorage.getItem('token')
			  }
			})
			.always(function(){
				app.ionicLoading.hide();	
			});
		}
	};

	return Service;
});