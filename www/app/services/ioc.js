define(["require"], function (require) {
	var map = {
		'userService': 'services/userService',
		'clientService': 'services/clientService',
		'appointmentService': 'services/appointmentService',
		'saleService': 'services/saleService',
		'profileService': 'services/profileService',
		'pandsService':'services/pandsService',
		'dashboardService':'services/dashboardService'
	};

	// use dummy
	var dummy = {
		// 'userService': 'dummy/dummyUserService',
		// 'clientService': 'dummy/clientServiceDummy',
		// 'appointmentService': 'dummy/appointmentServiceDummy',
		'saleService': 'dummy/saleServiceDummy',
		// 'profileService': 'dummy/profileServiceDummy',
		// 'pandsService':'dummy/pandsServiceDummy',
		// 'dashboardService':'dummy/dashboardServiceDummy'
	};
	$.extend(map, dummy);

	return {
		load: function(name, req, onLoad){			
			if(name in map)
				name = map[name];

			require([name], function(Service){
				if(typeof Service == "object")
					onLoad(Service);
				else
					onLoad(new Service());
			});
		}
	};
});