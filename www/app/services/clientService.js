define(['./http', 'common/filtering'], function(http, filtering){
	var Service = {
		searchClient: function(req){			
			return http.get('/api/clients')
			.then(function(r){
				return r.select(function(it){
					return {
						firstName: it.firstname,
						lastName: it.lastname,
						type: it.type.toLowerCase(),
						address: it.address,
						telephone1: it.phone,
						telephone2: it.mobilephone,
						email: it.email,
						note: it.note,
						id: it.id,
						date: moment(it.createdDate)
					};
				});
			})
			// client filter
			.then(function(r){
				if(!req)
					return r;
				if(req.text)
					r = r.where(function(it){
						return it.firstName.indexOf(req.text)>=0
						|| it.lastName.indexOf(req.text)>=0;
					});
				if(req.criterias && req.criterias.multiple)									
					r = filtering.filterMultiMatch(r, 'type', req.criterias.multiple);
				if(req.criterias && req.criterias.single)
					r = filtering.filterTime(r, 'date', req.criterias.single);

				return r;
			})
			;
		},
		createClient: function(req){			
			return http.post('/api/clients', {
                firstName: req.firstName,
                lastName: req.lastName,
                isClient: req.isClient,
                addressText: req.address.text,
                adressLatitude: req.address.latitude,
                adressLongitude: req.address.longitude,
                phone: req.telephone1,
                mobilephone: req.telephone2,
                email: req.email,
                note: req.note
            });
		}
	};

	return Service;
});