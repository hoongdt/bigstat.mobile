define(['./http'], function(http){
	// notImplementedException();
	// getAppointmentByMonth
	// getAppointmentByWeek
	// getAppointmentByDay
	// getAppointmentByHour
	var Service = {
		getAppointmentByHour: function(time){
			return http.get('/api/appointments', 
                {
                    status: ['InProgress','Rescheduled','Canceled','Absent','Shelved','ToBeContinued']
                })
			.then(function(r){
				return [{
					time: time,
					items: r.select(function (it) {
						return {
							time: moment(it.date),
							user: "{0} {1}".format(it.createdBy.firstName, it.createdBy.lastName),
							city: it.client.address.text,
							client: "{0} {1}".format(it.client.firstname, it.client.lastname),
							status: it.status,
							id: it.id
						};
					})
				}];
			});
		},
        addAppointment: function(item){
            return http.post('/api/appointments',item);
        },
        getAppointment: function(id){
        	return http.get('/api/appointments/'+id);
        },
        getAppointmentByMonth: function(){
        	return http.get('/api/appointments/getAppointmentByMonth')
        	.then(function (r) {
        		return r.select(function(it){
        			return {
        				time: moment(it.firstDate),
						rdv: it.nbAppointment,
						sale: it.nbSale,
						notSuite: it.nbSellerAppShelved,
						other: it.nbOtherApp,
						service: it.nbTechApp
        			};
        		});
        	});
        },
        getAppointmentByWeek: function(month){
        	return http.get('/api/appointments/getAppointmentByWeek/'+month.format("YYYY-MM-DD"))
        	.then(function (r) {
        		return r.select(function(it){
        			return {
        				time: moment(it.firstDate),
						rdv: it.nbAppointment,
						sale: it.nbSale,
						notSuite: it.nbSellerAppShelved,
						other: it.nbOtherApp,
						service: it.nbTechApp
        			};
        		});
        	});
        },
        getAppointmentByDay: function(day){
        	return http.get('/api/appointments/getAppointmentByDay/{0}/{1}'
        		.format(day.format("YYYY-MM-DD"), day.add(7,'days').format("YYYY-MM-DD")))
        	.then(function (r) {
        		return r.select(function(it){
        			return {
        				time: moment(it.firstDate),
						rdv: it.nbAppointment,
						sale: it.nbSale,
						notSuite: it.nbSellerAppShelved,
						other: it.nbOtherApp,
						service: it.nbTechApp
        			};
        		});
        	});
        }
	};

	return Service;
});