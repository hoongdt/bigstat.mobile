define(['./http', "app", 'common/formatter','dummy/dashboardServiceDummy'], 
	function(http,app,formatter,Dummy){
	var dummy = new Dummy();
	function mapTrend (it) {
		if(it=='Down')
			return 'down';
		if(it == 'Same')
			return  'same';
		return 'up';
	}
	var Service = {
		_getHeaderReq: null,
		_getHeader: function(){
			if(!this._getHeaderReq)
				this._getHeaderReq = http.get('/api/dashboards/getDashboardHeader/')
				.then(function (r) {
					var d = {
						name: r.companyName,
						agence: r.branchName,
						holdingAppointments: Number(r.objectiveVM.advancement).toFixed(0),
						objectivePercent: Number(!r.objectiveVM.objective?0
							:r.objectiveVM.advancement/r.objectiveVM.objective*100).toFixed(0),
						unit: r.objectiveVM.objectiveUnit,
						avatar: 'app/dummy/image/union.jpg',
						_source: r
					};
					if(app.usercontext.role == 'ceo'){
						d.leftRank = formatter.money(r.caDone, '€');
			    		d.leftRankStatus = mapTrend(r.caDoneTrend);
			    		d.leftRankTitle = 'CA tenu';
			    		d.rightRank = formatter.money(r.caCharged,'€');
			    		d.rightRankStatus = mapTrend(r.caChargedTrend);
			    		d.rightRankTitle = 'CA facturé';
					}

					return d;
				});
			return this._getHeaderReq;
		},
		getCompany: function(){
			return this._getHeader();
		},
		getRank: function (view) {
			return this._getHeader();
		},
		getPodium: function () {
			return dummy.getPodium();
		},
		_getMonthReq: null,
		_getMonthStat: function () {
			if(!this._getMonthReq)
				this._getMonthReq = http.get('/api/dashboards/getDashboardByMonth/'+moment().format("YYYY-MM-DD"));
			return this._getMonthReq;
		},
		_getYearReq: null,
		_getYearStat: function () {
			if(!this._getYearReq)
				this._getYearReq = http.get('/api/dashboards/getDashboardByYear/'+moment().format("YYYY-MM-DD"));
			return this._getYearReq;
		},
		_getDayReq: null,
		_getDayStat: function () {
			if(!this._getDayReq)
				this._getDayReq = http.get('/api/dashboards/getDashboardByDay/');
			return this._getDayReq;
		},
		getStat: function (category, mode) {
			// return dummy.getStat(category, mode);

			if(mode == 'day'){
				return this._getDayStat();
			}

			if(mode == 'month' || mode == 'year'){
				var genTimeName = function (it, idx) {
					return 'Semaine '+(idx+1);
				};
				var req = this._getMonthStat;
				if(mode == 'year'){
					genTimeName = function (it, idx) {
						return moment(it.startDate).format("MMMM");
					};
					req = this._getYearStat;
				}
				return req()
				.then(function(r){
					var ret = {
						name: category,
						_source: r
					};
					var set = {};
					if(category == 'appointment'){
						set = {
							itemsField: 'sellerAppDoneRateSet',
							valueField: 'nbAppDone',
							totalField: 'nbApp',
							type: 'percent',							
							nameFnc: genTimeName,
							summaryName: 'RDV'
						};
					}
					else if(category == 'holding')
						set = {
							itemsField: 'caDoneTotalSet',
							valueField: 'done',
							totalField: 'total',
							type: 'percent',
							nameFnc: genTimeName,
							summaryName: 'Tenu/total'
						};
					else if(category == 'pands')
						set = {
							itemsField: 'appAchievedRateByProductSet',
							valueField: 'nbTechAppDone',
							totalField: 'nbTechApp',
							type: 'percent',
							nameFnc: function(it, idx){
								return it.productName;
							},
							summaryName: 'Accomplie /total'
						};
					else if(category == 'facture')
						set = {
							itemsField: 'caChargedSet',
							valueField: 'ca',
							totalField: 'ca',
							nameFnc: genTimeName,
							summaryName: 'Facturées'
						};
					else if(category == 'stock'){
						set = {
							itemsField: 'productStockSet',
							valueField: 'productStock',
							totalField: 'productStock',
							nameFnc: function(it, idx){
								return it.productName;
							},
							summaryName: 'P&S'
						};
					}

					ret.items = r[set.itemsField]
					.select(function (it, idx) {
						return {
							name: set.nameFnc(it, idx),
							value: it[set.valueField],
							total: it[set.valueField],
							type: set.type
						}
					});
					var v = ret.items.sum(function (it) {
						return it.value;
					});
					var t = ret.items.sum(function (it) {
						return it.total;
					});
					ret.summary = {
						name: set.summaryName,
						value: v,
						total: t,
						type: set.type
					};
					
					return ret;
				});
			}
		}
	};

	return Service;
});