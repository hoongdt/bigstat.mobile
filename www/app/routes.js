define([
    'app'    
], function(app) {
    'use strict';
    // definition of routes
    app.config([
        '$stateProvider',
        '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider) {
            // url routes/states
            $urlRouterProvider.otherwise('/splash');

            $stateProvider
            .state('splash', {
                url: '/splash',                
                templateUrl: 'app/views/splash.html',
                controller: 'splash'
            })
            .state('login', {
                url: '/login',                
                templateUrl: 'app/views/login.html',
                controller: 'login'
            })
            .state('tab', {
                url: '/tab',
                abstract: true,
                templateUrl: 'app/views/tabs.html',
                controller: 'tabs'
            })
            .state('tab.dashboard', {
                url: '/dashboard',
                views: {
                    'dashboard': {
                        templateUrl: 'app/views/dashboard.html',
                        controller: 'dashboard'
                    }
                }
            })
            .state('tab.agenda', {
                url: '/agenda',
                views: {
                    'agenda': {
                        templateUrl: 'app/views/agenda.html',
                        controller: 'agenda'
                    }
                }
            })
            .state('tab.prize', {
                url: '/prize',
                views: {
                    'prize': {
                        templateUrl: 'app/views/prize.html',
                        controller: 'prize'
                    }
                }
            })
            .state('tab.profile', {
                url: '/profile/:id',
                views: {
                    'prize': {
                        templateUrl: 'app/views/profile.html',
                        controller: 'profile'
                    }
                }
            })
            .state('tab.search', {
                url: '/search/:view',
                views: {
                    'search': {
                        templateUrl: 'app/views/search.html',
                        controller: 'search'
                    }
                }
            })
            .state('tab.client-create', {
                url: '/client-create',
                views: {
                    'search': {
                        templateUrl: 'app/views/client-create.html',
                        controller: 'clientCreate'
                    }
                }
            })         
            .state('tab.appointment-create', {
                url: '/appointment-create',
                views: {
                    'search': {
                        templateUrl: 'app/views/appointment-create.html',
                        controller: 'appointmentCreate'
                    }
                }
            })
            .state('tab.seller', {
                url: '/seller/:id/:view',
                views: {
                    'search': {
                        templateUrl: 'app/views/seller.html',
                        controller: 'seller'
                    }
                }
            })
            .state('tab.search-list-filter', {
                url: '/filter/:view',
                views: {
                    'search': {
                        templateUrl: 'app/views/list-filter.html',
                        controller: 'listFilter'
                    }
                }
            })
            .state('tab.prize-list-filter', {
                url: '/filter/:view',
                views: {
                    'prize': {
                        templateUrl: 'app/views/list-filter.html',
                        controller: 'listFilter'
                    }
                }
            })
            .state('tab.pands-picker', {
                url: '/pands-picker',
                views: {
                    'search': {
                        templateUrl: 'app/views/pands-picker.html',
                        controller: 'pandsPicker'
                    }
                }
            })

            .state('tab.client-picker', {
                url: '/client-picker',
                views: {
                    'search': {
                        templateUrl: 'app/views/client-picker.html',
                        controller: 'clientPicker'
                    }
                }
            })
            .state('tab.profile-picker', {
                url: '/profile-picker/:category/:multi',
                views: {
                    'search': {
                        templateUrl: 'app/views/profile-picker.html',
                        controller: 'profilePicker'
                    }
                }
            })
            ;
        }
    ]);
});
