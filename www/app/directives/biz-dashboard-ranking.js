define(['app','ioc!dashboardService'], function(app, service){
	app.directive('bizDashboardRanking', function(){
		return {
			restrict: 'AE',
			templateUrl: 'app/views/biz-dashboard-ranking.html',
			scope: {
				bizRole:'@'
			},
			controller: ['$scope' , '$rootScope', function($scope, $root) {
		    	var vm ={};
		    	service.getRank($root.usercontext.role).done(function(r){
		    		$.extend(vm, r);
		    	});
		    	$scope.vm = vm;
		    }]
		};
	});
});