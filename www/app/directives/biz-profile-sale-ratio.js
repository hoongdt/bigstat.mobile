define(['app', 'ioc!profileService'], function(app, service){
	app.directive('bizProfileSaleRatio', function(){
		return {
			restrict: 'AE',
			templateUrl: 'app/views/biz-profile-sale-ratio.html',
			scope: {
				bizClientSaleValue:'@'
			},
			controller: ['$scope' , '$state', function($scope, $state) {
		    	var vm ={
		    		percent: 70
		    	};
		    	$scope.vm = vm;		    	
		    }]
		};
	});
});