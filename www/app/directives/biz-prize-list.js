define(['app', 'ioc!profileService'], function(app, service){
	app.directive('bizPrizeList', function(){
		return {
			restrict: 'AE',
			templateUrl: 'app/views/prize-list.html',
			scope: {
				bizRole:'@',
				bizForPicker: '@',
				bizItems: '='
			},
			controller: ['$scope' , '$state', function($scope, $state) {
				$scope.bizItems = [];
		    	var vm ={
		    		items:$scope.bizItems,
		    		role: $scope.bizRole,
		    		forPicker: $scope.bizForPicker,
		    		multiPicker: $state.params.multi,
		    		gotoProfile: function(profile){
		    			if(this.forPicker && !this.multiPicker){
                			$scope.$parent.onSelect(profile);
		    				return;
		    			}

		    			if(this.forPicker && this.multiPicker){
		    				return;
		    			}

		    			$state.go('tab.profile',{id: profile.id});
		    		}		    		
		    	};
		    	$scope.vm = vm;
		    	function fetchdata (filter) {
	                service.getList(vm.role, filter).done(function(r){
	                    vm.items.length = 0;
			    		vm.items.addRange(r);
			    		$scope.$apply();
			    	});
	            }
	            fetchdata();
	            $scope.onFilter = function(filter){
	                fetchdata(filter);
	            };
		    	
		    }]
		};
	});
});