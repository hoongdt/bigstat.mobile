define(['app', 'ioc!profileService'], function(app, service){
	app.directive('bizProfileCaChart', function(){
		return {
			restrict: 'AE',
			templateUrl: 'app/views/biz-profile-ca-chart.html',
			scope: {
				bizData:'@'
			},
			controller: ['$scope' , function($scope) {
		    	var vm ={
		    		data: [[]],
                    labels: [],
                    months: [],
                    chosen: function(m){
                    	return this._month == m;

                    },
                    _month: null,
                    choose: function(m){
                    	this._month = m;
                    },
                    refresh: function(){
                    	loadData();
                    }
		    	};

		    	function loadData () {
		    		service.getCaData().done(function(r){
						vm.data = [r.select(function(it){
							return it.value;
						})];
						vm.labels = r.select(function(it){
							return "";
						});
						vm.months = r.select(function(it){
							return it.time;
						});
					});
		    	}
		    	loadData();

		    	$scope.vm = vm;
		    }]
		};
	});
});