define(['app', 'ioc!profileService'], function(app, service){
	app.directive('bizPercentageCircle', function(){
		return {
			restrict: 'AE',
			scope: {
				bizValue: "@"
			},
			link: function(scope, elem, attrs){
				var c=elem[0];
				var ctx=c.getContext('2d');
				ctx.lineWidth = 3;
				ctx.strokeStyle = "#80CFF9";
			    ctx.beginPath();
				ctx.arc(50,50,48,0.5*Math.PI,(0.5+2*scope.bizValue/100)*Math.PI);
				ctx.stroke();
			}
		}
	});

	app.directive('bizProfileProductRank', function(){
		return {
			restrict: 'AE',
			templateUrl: 'app/views/biz-profile-product-rank.html',
			scope: {				
			},
			controller: ['$scope' , '$state', function($scope, $state) {
		    	var vm ={
		    		products: [],
		    		avatarStyle: function(it){
		    			var size = 100;
		    			return {
		    				'background-image': 'url({0})'.format(it.seller.avatar),
		    				'height': size-20,
		    				'width': size-20,
		    				'margin-top': 100-size
		    			};
		    		},
		    		containerWidth: function(){
		    			var that = this;
		    			// return this.products.length*(100+20);
		    			return this.products.sum(function(it){
		    				return 100*that.avatarScale(it)+20;
		    			});

		    		},
		    		avatarScale: function(it){
		    			var size = 100;
		    			if(it.value>=70)
		    				size = 100;
		    			else if(it.value<70 && it.value >=50 )
		    				size = 80;
		    			else if(it.value<50 )
		    				size = 60;
		    			return size/100;
		    		},
		    		circleTransform: function(it){
		    			var ratio = this.avatarScale(it);
		    			return 'transform: scale({0}) translateY({1}px) translateX(-{1}px)'.format(ratio, (100-100*ratio)/2);
		    		}
		    	};
		    	service.getProductRank().done(function(r){
		    		vm.products = r;
		    	});
		    	$scope.vm = vm;		    	
		    }]
		};
	});
});