define(['app','ioc!dashboardService'], function(app, service){
	app.directive('bizDashboardPodium', function(){
		return {
			restrict: 'AE',
			templateUrl: 'app/views/biz-dashboard-podium.html',
			scope: {
				bizMode:'@'
			},
			controller: ['$scope' ,'$rootScope', function($scope,$root) {
		    	var vm ={
		    		categories: [{
		    			name: 'director'
		    		},{
		    			name: 'commercial'
		    		},{
		    			name: 'prospector'
		    		},{
		    			name: 'service-agent'
		    		}],
		    		title: function(it){
		    			if($scope.bizMode == 'day')
		    				return "Podium {0} du jour".format(it.name)
		    			.replaceAll('director', 'directeur')
		    			.replaceAll('prospector', 'prospecteur')
		    			.replaceAll('service-agent', 'agent de service');
		    			if($scope.bizMode == 'month')
		    				return "Podium {0} du mois".format(it.name)
		    			.replaceAll('director', 'directeur')
		    			.replaceAll('prospector', 'prospecteur')
		    			.replaceAll('service-agent', 'agent de service');
		    			return "Podium {0}".format(it.name);
		    		}
		    	};
		    	if($root.usercontext.role == 'prospector'
		    		|| $root.usercontext.role == 'commercial'){
		    		vm.categories.removeItems(function(it){
		    			return it.name!=$root.usercontext.role;
		    		});
		    	}
		    	service.getPodium()
		    	.done(function(r){
		    		for (var i = 0; i < vm.categories.length; i++) {
		    			var cat = vm.categories[i];
		    			cat.items = r[i].items;
		    			cat.more = r[i].more;
		    		};
		    	});
		    	$scope.vm = vm;
		    }]
		};
	});
});