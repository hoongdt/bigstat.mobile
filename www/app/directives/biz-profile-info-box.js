define(['app'], function(app){
	app.directive('bizProfileInfoBox', function(){
		return {
			restrict: 'AE',
			templateUrl: 'app/views/biz-profile-info-box.html',
			scope: {
				bizData:'@'
			},
			controller: ['$scope' , function($scope) {
		    	var vm ={
		    		rows: []  		
		    	};

		    	var items = eval($scope.bizData);
		    	var rc = items.length/3;
		    	for (var i = 0; i < rc; i++) {
		    		var r = [];
		    		vm.rows.push(r);
		    		var idx = i*3;
		    		if(idx>=items.length)
		    			continue;
		    		r.push(items[idx]);
		    		var idx = i*3+1;
		    		if(idx>=items.length)
		    			continue;
		    		r.push(items[idx]);
		    		var idx = i*3+2;
		    		if(idx>=items.length)
		    			continue;
		    		r.push(items[idx]);
		    	};

		    	$scope.vm = vm;
		    }]
		};
	});
});