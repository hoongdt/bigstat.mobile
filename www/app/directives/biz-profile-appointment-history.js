define(['app', 'ioc!profileService'], function(app, service){
	app.directive('bizProfileAppointmentHistory', function(){
		return {
			restrict: 'AE',
			templateUrl: 'app/views/biz-profile-appointment-history.html',
			scope: {
				bizProfile:'@'
			},
			controller: ['$scope' , function($scope) {
		    	var vm ={
		    		appointments:[],
		    		containerWidth: function(){
		    			return this.appointments.sum(function(r){
		    				return 60+20;
		    			});
		    		}
		    	};
		    	$scope.vm = vm;		    	
		    	service.getLastAppointments()
		    	.done(function(r){
		    		vm.appointments.addRange(r);
		    	});
		    }]
		};
	});
});