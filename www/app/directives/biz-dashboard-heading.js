define(['app', 'ioc!dashboardService'], function(app, service){
	app.directive('bizDashboardHeading', function(){
		return {
			restrict: 'AE',
			templateUrl: 'app/views/biz-dashboard-heading.html',
			scope: {
				bizRole:'@'
			},
			controller: ['$scope' , '$state', function($scope, $state) {
		    	var vm ={
		    		company: null
		    	};
		    	service.getCompany().done(function(r){
		    		vm.company = r;
		    	});
		    	$scope.vm = vm;
		    }]
		};
	});
});