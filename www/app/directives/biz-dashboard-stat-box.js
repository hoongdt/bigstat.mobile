define(['app', 'ioc!dashboardService'], function(app, service){
	app.directive('bizDashboardStatBox', function(){
		return {
			restrict: 'AE',
			templateUrl: 'app/views/biz-dashboard-stat-box.html',
			scope: {
				bizStatName:'@',
				bizMode:'@'
			},
			controller: ['$scope' , function($scope) {
		    	var vm ={
		    		items: [],
		    		summary:null,
		    		statName: $scope.bizStatName
		    	};
		    	service.getStat($scope.bizStatName, $scope.bizMode)
		    	.done(function(r){
		    		vm.summary = r.summary;
		    		vm.items = r.items;
		    	});
		    	$scope.vm = vm;
		    }]
		};
	});
});