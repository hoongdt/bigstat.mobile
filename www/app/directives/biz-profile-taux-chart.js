define(['app', 'ioc!profileService'], function(app, service){
	app.directive('bizProfileTauxChart', function(){
		return {
			restrict: 'AE',
			templateUrl: 'app/views/biz-profile-taux-chart.html',
			scope: {
				bizProfile:'='
			},
			controller: ['$scope' , function($scope) {
		    	var vm ={
		    		data: [[]],
                    labels: [],
                    months: [],
                    chosen: function(m){
                    	return this._month == m;

                    },
                    _month: null,
                    choose: function(m){
                    	this._month = m;
                    },
                    refresh: function(){
                    	loadData();
                    },
                    options: []
		    	};
		    	if($scope.bizProfile && $scope.bizProfile.category == 'service-agent')
		    		vm.options = [
		    		"Accomplissement", 
		    		"SAV"];	
		    	else if($scope.bizProfile && $scope.bizProfile.category == 'prospector')
		    		vm.options = [
		    		"RDV fiables", 
		    		"RDV vendu"];		    	
		    	else if($scope.bizProfile && $scope.bizProfile.category == 'commercial')
		    		vm.options = [
		    		"Réussite sur RDV prospect",
                    "Réussite sur RDV client",
                    "Annulée",
                    "Financement",
                    "Fiabilité RDV"];	
                else if($scope.bizProfile && $scope.bizProfile.category == 'director')
		    		vm.options = [
		    		"Réussite sur RDV prospect",
                    "Réussite sur RDV client",
                    "Annulée",
                    "Financement",
                    "Fiabilité"];		
                else if($scope.bizProfile && $scope.bizProfile.category == 'agence')
		    		vm.options = [
                    "Réussite sur RDV prospect",
                    "Réussite sur RDV client",
                    "Annulée",
                    "Financement",
                    "Fiabilité",
                    "Accomplissement",
                    "SAV"];	    	

		    	function loadData () {
		    		service.getTauxData().done(function(r){
						vm.data = [r.select(function(it){
							return it.value;
						})];
						vm.labels = r.select(function(it){
							return "";
						});
						vm.months = r.select(function(it){
							return it.time;
						});
					});
		    	}
		    	loadData();

		    	$scope.vm = vm;
		    }]
		};
	});
});