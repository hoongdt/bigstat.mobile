define(['app', 'ioc!dashboardService'], function(app, service){
	app.directive('bizDashboardAppointmentSummaryBox', function(){
		return {
			restrict: 'AE',
			templateUrl: 'app/views/biz-dashboard-appointment-summary-box.html',
			scope: {
			},
			controller: ['$scope' , '$state', function($scope, $state) {
				var vm ={
		    		company: null,
		    		labels: ["Vente", "RDV tenu", "Autres"],
		    		data: [300, 500, 100]
		    	};
			    	
		    	$scope.vm = vm;
		    }]
		};
	});
});