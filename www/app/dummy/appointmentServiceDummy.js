define(['./baseDummy'],function(Dummy){
	var Service = function(){
		Dummy.call(this);
		this.getAppointmentByMonth = function () {
			return this.out([0,10].increase(function(i){
				return {
					time: moment().subtract(i,'months'),
					rdv: [10,100].random(),
					sale: [10,100].random(),
					notSuite: [10,100].random(),
					other: [10,100].random(),
					service: [10,100].random()
				};
			}),500);
		};
		this.getAppointmentByWeek = function(){
			return this.out([0,10].increase(function(i){
				return {
					time: moment().subtract(i*7,'days'),
					rdv: [10,100].random(),
					sale: [10,100].random(),
					notSuite: [10,100].random(),
					other: [10,100].random(),
					service: [10,100].random()
				};
			}),500);
		};
		this.getAppointmentByDay = function(){
			return this.out([0,10].increase(function(i){
				return{
					time: moment().subtract(i,'days'),
					rdv: [10,100].random(),
					sale: [10,100].random(),
					notSuite: [10,100].random(),
					other: [10,100].random(),
					service: [10,100].random(),
					items: [0,5].increase(function(i){
						return {
							time: moment().subtract(i,'hours'),
							rdv: [10,100].random(),
							sale: [10,100].random(),
							notSuite: [10,100].random(),
							other: [10,100].random(),
							service: [10,100].random()
						};					
					})
				}
			}),500);
		};
		this.getAppointmentByHour = function(){
			return this.out([0,1].increase(function(i){
				return{
					time: moment().subtract(i,'days'),
					items: [0,20].increase(function(i){
						return {
							time: moment().subtract(i*10+i,'minutes'),
							user: ["DURAND SYLVIE", "HOONG DANG"].random(),
							city: ["Le Mans", "Versaille", "Hanoi", "Paris"].random(),
							client: ["Ha. Gandonn.", "Julien Geffray", "ulien Geffra"].random(),
							status: ["confirmed", "sale", "reported", "absent", "on-service","preview-service"].random(),
							id: -1
						};					
					})
				}
			}),500);
		};
		this.getAppointment = function(id){
			return this.out({
				id: id,
				name: "HERO",
				events: [
					{
						time: moment(),
						client: ["H. Gandonnière", "H. Gandonnière", "P. Henry"].random(),
						type: 1
					},
					{
						time: moment().add(-3, "days"),
						client: ["H. Gandonnière", "H. Gandonnière", "P. Henry"].random(),
						type: 2
					},
					{
						time: moment().add(-7, "days"),
						client: ["H. Gandonnière", "H. Gandonnière", "P. Henry"].random(),
						type: 3
					}
				]

			}, 300);
		}
	};

	Service.prototype = Object.create(Dummy.prototype);

	return Service;
});