define(['./baseDummy'],function(Dummy){
	var Service = function(){
		Dummy.call(this);
		this.getCompany = function(){
			return this.out({
				name: 'UNION OUEST HABITAT',
				agence: 'LE MANS',
				holdingAppointments: 78,
				objectivePercent: 64,
				avatar: 'app/dummy/image/union.jpg'
			}, 500);
		};

		this.getRank = function(view){
			if(view=='ceo'){
				return this.out({
		    		leftRank: '1,5M€',
		    		leftRankStatus: ['same','up','down'].random(),
		    		leftRankTitle: 'CA tenu',
		    		rightRank:'780k€',
		    		rightRankStatus: ['same','up','down'].random(),
		    		rightRankTitle: 'CA facturé'
		    	}, 500);
			}
			return this.out({
	    		leftRank: [1,10].random()+'eme',
	    		leftRankStatus: ['same','up','down'].random(),
	    		leftRankTitle: 'Classement agence',
	    		rightRank:[1,10].random()+'eme',
	    		rightRankStatus: ['same','up','down'].random(),
	    		rightRankTitle: 'Classement perso'
	    	}, 500);
		};

		this.getPodium = function(){
			return this.out(
				[{
					name: 'director',
					items: [0,3].increase(function(it){
						return {
							avatar: 'app/dummy/image/{0}.jpg'.format(["steve-job", "tony-stark", "dicaprio", "dicaprio"].random())
						}
					}),
					more: [1,5].random()
				},
				{
					name: 'commercial',
					items: [0,3].increase(function(it){
						return {
							avatar: 'app/dummy/image/{0}.jpg'.format(["steve-job", "tony-stark", "dicaprio", "dicaprio"].random())
						}
					}),
					more: [1,5].random()
				},
				{
					name: 'prospector',
					items: [0,3].increase(function(it){
						return {
							avatar: 'app/dummy/image/{0}.jpg'.format(["steve-job", "tony-stark", "dicaprio", "dicaprio"].random())
						}
					}),
					more: [1,5].random()
				},
				{
					name: 'service-agent',
					items: [0,3].increase(function(it){
						return {
							avatar: 'app/dummy/image/{0}.jpg'.format(["steve-job", "tony-stark", "dicaprio", "dicaprio"].random())
						}
					}),
					more: [1,5].random()
				}
				]
			, 500);
		};
		this.getStat = function(category, mode){
			if(mode == 'month' && category == 'appointment')
				return this.out({
					name: category,				
					summary: {
						name: 'RDV',
						value: 32
					},
					items: [1,6].increase(function(i){
						return {
							name: 'Semaine '+i,
							value: [1,15].random(),
							total: [15,20].random(),
							type: 'percent'
						}
					})
				},500); 

			if(mode == 'month' && category == 'holding')
				return this.out({
					name: category,				
					summary: {
						name: 'Tenu/total',
						value: '47/58k€'
					},
					items: [1,6].increase(function(i){
						return {
							name: 'Semaine '+i,
							value: [1,15].random(),
							total: [15,20].random(),
							type: 'percent'
						}
					})
				},500); 

			if(mode == 'month' && category == 'facture')
				return this.out({
					name: category,				
					summary: {
						name: 'Facturées',
						value: '48,5k€'
					},
					items: [1,6].increase(function(i){
						return {
							name: 'Semaine '+i,
							value: [1,50].random()+"k€"
						}
					})
				},500); 

			if(mode == 'year' && category == 'appointment')
				return this.out({
					name: category,				
					summary: {
						name: 'RDV',
						value: 32
					},
					items: [0,12].increase(function(i){
						return {
							name: moment().add(i,'months').format('MMMM'),
							value: [1,15].random(),
							total: [15,20].random(),
							type: 'percent'
						}
					})
				},500); 

			if(mode == 'year' && category == 'holding')
				return this.out({
					name: category,				
					summary: {
						name: 'Tenu/total',
						value: '47/58k€'
					},
					items: [0,9].increase(function(i){
						return {
							name: moment().add(i,'months').format('MMMM'),
							value: [1,15].random(),
							total: [15,20].random(),
							type: 'percent'
						}
					})
				},500); 

			if(mode == 'year' && category == 'facture')
				return this.out({
					name: category,				
					summary: {
						name: 'Facturées',
						value: '48,5k€'
					},
					items: [0,10].increase(function(i){
						return {
							name: moment().add(i,'months').format('MMMM'),
							value: [1,50].random()+"k€"
						}
					})
				},500); 
			if(mode=='month' && category=='pands'){
				var stack = new Stack(["Ouate de cellu.","VMI","Hydrofuge","Façade","SAV"]);
				return this.out({
					name: category,				
					summary: {
						name: 'Accomplie /total',
						value: '33/3'
					},
					items: [0,stack.length()].increase(function(i){
						return {
							name: stack.top(),
							value: [1,15].random(),
							total: [15,20].random(),
							type: 'percent'
						}
					})
				},500); 
			}

			if(mode == 'month' && category == 'ca')
				return this.out({
					name: category,				
					summary: {
						name: 'CA',
						value: '75k€'
					},
					items: [1,6].increase(function(i){
						return {
							name: 'Semaine '+i,
							value: [1,15].random(),
							total: [15,20].random(),
							type: 'percent'
						}
					})
				},500); 

			if(mode=='month' && category=='stock'){
				var stack = new Stack(["Ouate de cellu.","VMI","Hydrofuge","Façade","SAV"]);
				return this.out({
					name: category,				
					summary: {
						name: 'P&S',
						value: 'stock'
					},
					items: [0,stack.length()].increase(function(i){
						return {
							name: stack.top(),
							value: [1,600].random()
						}
					})
				},500); 
			}
		};
	};

	Service.prototype = Object.create(Dummy.prototype);

	return Service;
});