define(["app"],function(app){
	var Service = function(){	
	};
	Service.prototype.out = function(data, timeout) {
		var ret = $.Deferred();
		if(timeout){
			app.ionicLoading.show();
			setTimeout(function() {
				ret.resolve(data);
				app.ionicLoading.hide();	
			}, timeout);
		}
		else
			ret.resolve(data);
		return ret;
	};

	return Service;
});