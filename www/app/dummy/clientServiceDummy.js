define(["./baseDummy"], function(Dummy){
	var Service = function(){
		Dummy.call(this);
		this.searchClient = function(){
			return this.out([0, 20].increase(function(){
				return {
					city: ["Le Mans", "Versaille", "Hanoi", "Paris"].random(),
					type: ["client", "prospect", "retracted"].random(),
					name: 'DURAND SYLVIE',
				};
			}), 500);			
		};
	};

	Service.prototype = Object.create(Dummy.prototype);

	return Service;
});