define(['./baseDummy'],function(Dummy){
	var Service = function(){
		Dummy.call(this);
		this.getSaleByMonth = function () {
			return this.out([0,10].increase(function(i){
				return {
					time: moment().subtract(i,'months'),
					pendingSale: [10,100].random(),
					invoiceSale: [10,100].random(),
					performedService: [10,100].random(),
					otherSale: [10,100].random()					
				};
			}),500);
		};
		this.getSaleByWeek = function(){
			return this.out([0,10].increase(function(i){
				return {
					time: moment().subtract(i*7,'days'),
					pendingSale: [10,100].random(),
					invoiceSale: [10,100].random(),
					performedService: [10,100].random(),
					otherSale: [10,100].random()
				};
			}),500);
		};
		this.getSaleByDay = function(){
			return this.out([0,2].increase(function(i){
				return{
					time: moment().subtract(i,'days'),
					pendingSale: [10,100].random(),
					invoiceSale: [10,100].random(),
					performedService: [10,100].random(),
					otherSale: [10,100].random(),					
					items: [0,3].increase(function(i){
						return {
							time: moment().subtract(i,'hours'),							
							user: 'DURAND',							
							customer: 'Le Mans',							
							total: [10,100].random(),
							status: ["no", "invoiced", "canceled", "service-provided", "current-service","service-completed"].random(),
						};					
					})
				}
			}),500);
		};

		this.getSaleAsServices = function() {
			return this.out([0,2].increase(function(i){
				return{
					time: moment().subtract(i,'days'),
					
					status: [10,100].random(),
					otherSale: [10,100].random(),					
					items: [0,2].increase(function(i){
						return {
							serviceName: ["Ouate de cellulose","Hydrofuge incolore"].random(),
							quantity: [10,100].random(),							
							stock: [-10,10].random(),
							status: ["no", "invoiced", "canceled", "service-provided", "current-service","service-completed"].random(),
						};					
					})
				}
			}),500);			
		}

		this.getSale = function(id){
			return this.out({
				id: id,
				name: "HERO"
			}, 300);
		};

		this.getPendingSale = function(id){
			return this.out({
				id:id,
				time: moment().subtract(2,'days'),
				items: [0,2].increase(function(i){
					return  {
						title:["Ouate de cellulose","Hydrofuge incolore"].random(),
						quantity : [10,100].random(),
						amount:[10,100].random(),
						pose:[10,100].random()
					};
				})
			},500);
		};

		this.getAdditionalPendingSale = function(id){
			return this.out({
				id:id,
				saleTime: moment().subtract(2,'day'),
				time: moment(),
				items: [0,2].increase(function(i){
					return  {
						title:["Ouate de cellulose","Hydrofuge incolore"].random(),
						quantity : [10,100].random(),
						amount:[10,100].random(),
						pose:[10,100].random()
					};
				})
			},500);
		};

	};

	Service.prototype = Object.create(Dummy.prototype);

	return Service;
});