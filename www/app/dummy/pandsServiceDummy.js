define(['./baseDummy'],function(Dummy){
	var Service = function(){
		Dummy.call(this);
		this.getList = function(){
			return this.out([0,10].increase(function(i){
				return {
					name: ["Façade", "Hydrofuge", "Ouate de cellulose", "VMI"].random(),
					quantity: [10,1200].random(),
					price: [9000,120000].random()
				};
			}),500);
		};
	};

	Service.prototype = Object.create(Dummy.prototype);

	return Service;
});