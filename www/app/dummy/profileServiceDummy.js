define(["./baseDummy", 'moment'], function(Dummy, moment){
	var Service = function(){
		Dummy.call(this);
		var data= localStorage.getItem('dummy-profiles');
		if(data){
			data = JSON.parse(data);
			data.loop(function(it){
				it.date = moment(it.date);
			});
		}

			
		else{
			data = [0, 50].increase(function(i){
				return {
					firstName:["HOONG", "JOHN", "BRITNEY", "ALBERT", "BILL"].random(),
					lastName:["DANG", "LENNON", "SPEAR", "STEINEN", "GATES"].random(),
					status: [1, 2, 3, 4, 5].random(),
					feeling: ["relaxed", "curious", "romantic", "happy", "general"].random(),
					bigz: ["How are you?", "Where are you from?", "I like it"].random(),
					category: ['prospector', 'director', 'commercial', 'service-agent','agence'].random(),
					avatar: 'app/dummy/image/{0}.jpg'.format(["steve-job", "tony-stark", "dicaprio", "dicaprio"].random()),
					date: moment().add(-[1, 6000].random(), 'minutes'),
					id: i
				}}
			);	
			localStorage.setItem('dummy-profiles', JSON.stringify(data));
		}


		this.getList = function(category){
			return this.out(data.where(function(it){
				return it.category == category;
			}), 500);	
		};
		this.getProfile = function(id){
			return this.out(data.firstOrNull(function(r){
				return r.id == id;
			}))
		};
		this.getCaData = function(profileId){
			return this.out([-6,0].increase(function(i){
				return {
					time: moment().add(i,'months'),
					value: [10,100].random()
				};
			}), 500);
		};

		this.getTauxData = function(profileId){
			return this.out([-4,0].increase(function(i){
				return {
					time: moment().add(i,'months'),
					value: [10,100].random()
				};
			}), 500);
		};

		this.getProductRank= function(){			
			var stack = new Stack(["steve-job", "tony-stark", "dicaprio", "dicaprio"]);
			var stack1 = new Stack([[70,90].random(), [50,60].random(), [30,40].random(), 30]);
			return this.out([0,4].increase(function(i){
				return {
					name: ["Ouate de cel.", "Façade" , "VMI", "Hyd."].random(),
					value: stack1.top(),
					seller: {
						avatar: 'app/dummy/image/{0}.jpg'.format(stack.top()),
						name: null
					}
				};
			}), 500);
		};

		this.getLastAppointments = function(){			
			var lst = ["confirmed","confirmed","sale","negotiate", "canceled"].select(function(it){
				return {status: it}
			});
			lst.addRange([0,5].increase(function(i){
				return {
					status: ["confirmed", "sale", "negotiate","canceled"].random()
				};
			}));
			return this.out(lst);
		}
	};

	Service.prototype = Object.create(Dummy.prototype);

	return Service;
});