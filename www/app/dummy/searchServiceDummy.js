define(['./baseDummy'],function(Dummy){
	var Service = function(){
		Dummy.call(this);
		this.searchClient = function(){
			return this.out([0, 20].increase(function(){
				return {
					city: ["Le Mans", "Versaille", "Hanoi", "Paris"].random(),
					type: ["client", "prospect", "retracted"].random(),
					name: 'DURAND SYLVIE',
				};
			}), 500);			
		};
		this.getRdvByMonth = function () {
			return this.out([0,10].increase(function(i){
				return {
					time: moment().subtract(i,'months'),
					rdv: [10,100].random(),
					sale: [10,100].random(),
					notSuite: [10,100].random(),
					other: [10,100].random(),
					service: [10,100].random(),
					items: [0,5].increase(function(i){
						return {
							time: moment().subtract(i,'hour'),
							rdv: [10,100].random(),
							sale: [10,100].random(),
							notSuite: [10,100].random(),
							other: [10,100].random(),
							service: [10,100].random(),
							isTotal: i==4 
						};
						
					})
				};
			}),500);
		};
	};

	Service.prototype = Object.create(Dummy.prototype);

	return Service;
});