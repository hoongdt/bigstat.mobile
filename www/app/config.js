define([
  'app'
], function (app) {
  'use strict';
  // additional config-blocks
  app.run([
  	"$ionicLoading",
  	"$state",
    function ($ionicLoading, $state) {
    	app.ionicLoading = $ionicLoading;
    	app.$state = $state;
    }
  ]);
  app.config(function ($ionicConfigProvider) {
      $ionicConfigProvider.tabs.position('bottom')
  });
});
