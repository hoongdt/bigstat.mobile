// requires routes, config, run they implicit requiring the app
require([
  'moment',
  'jquery',
  'controllers/_controllers',
  'filters/_filters',
  'directives/_directives',
  'routes',
  'config',
  'run',
  'common/linq.array',
  'common/utils',
  'common/exceptions',
  'gmap'
], function (moment) {
  'use strict';
  moment.locale('fr');
  window.moment = moment;
  // Here you have to set your app name to bootstrap it manually
  angular.bootstrap(document, ['app']);
});
