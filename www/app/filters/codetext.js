define([
    'app'
], function(app) {
    'use strict';
    app.filter('codetext', [
    	function () {
	    	var Filter = function(val, arg) {
	    		if(val == 'prospect')
	    			return 'Prospect';
	    		if(val == 'client')
	    			return 'Client';
	    		if(val == 'today')
	    			return "Aujourd’hui";
	    		if(val == 'thisweek')
	    			return "Cette semaine";
	    		if(val == 'thismonth')
	    			return "Ce mois ci";
	    		if(val == 'nofilter')
	    			return "Indifférent";

	    		
	    		return val;
	        }
	        return Filter;	
    	}        
    ]);
});