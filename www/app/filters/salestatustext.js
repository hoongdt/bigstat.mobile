define([
    'app'
], function(app) {
    'use strict';
    app.filter('salestatustext', [
    	function () {
	    	var Filter = function(val, arg) {
	    		if(val == 'no')
	    			return "";
	    		if(val == 'invoiced')
	    			return "Facturée";
	    		if(val == 'canceled')
	    			return "Annulée";
	    		if(val == 'service-provided')
	    			return "Service prévu";
	    		if(val == 'current-service')
	    			return "Service en cours";
	    		if(val == 'service-completed')
	    			return "Service accomplie";  		
	        	return val;
	        }
	        return Filter;	
    	}        
    ]);
});