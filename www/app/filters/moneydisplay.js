define([
    'app',
    'common/formatter'
], function(app, formatter) {
    'use strict';
    app.filter('moneydisplay', [
    	function () {
	    	var Filter = function(val, arg) {
	    		return formatter.money(val);
	        }
	        return Filter;	
    	}        
    ]);
});