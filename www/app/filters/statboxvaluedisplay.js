define([
    'app',
    'common/formatter'
], function(app, formatter) {
    'use strict';
    app.filter('statboxvaluedisplay', [
    	function () {
	    	var Filter = function(val, arg) {
                if(!val)
                    return val;
                
                if(arg == 'appointment'){
                    return "{0}/{1}".format(val.value, val.total);
                }
                if(arg == 'holding'){
                    return "{0}/{1}€".format(formatter.money(val.value), formatter.money(val.total));
                }
                if(arg == 'pands'){
                    return "{0}/{1}".format(formatter.money(val.value), formatter.money(val.total));
                }
                if(arg == 'facture'){
                    return formatter.money(val.value, '€');
                }
                if(arg == 'stock'){
                    return val.value;
                }
                
                
	    		return val;
	        }
	        return Filter;	
    	}        
    ]);
});