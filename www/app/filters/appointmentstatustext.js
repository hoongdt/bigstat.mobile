define([
    'app'
], function(app) {
    'use strict';
    app.filter('appointmentstatustext', [
    	function () {
	    	var Filter = function(val, arg) {
	    		if(val == 'confirmed' || val=='InProgress')
	    			return "En cours (confirmé)";
	    		if(val == 'sale')
	    			return "Vente";
	    		if(val == 'reported')
	    			return "Reporté";
	    		if(val == 'absent')
	    			return "Absent";
	    		if(val == 'on-service')
	    			return "Service en cours";
	    		if(val == 'on-service')
	    			return "Service en cours";
	    		if(val == 'preview-service')
	    			return "Service prévu";
	        	return val;
	        }
	        return Filter;	
    	}        
    ]);
});
