define([
    'app'
], function(app) {
    'use strict';
    app.filter('clienttypefilter', [
    	function () {
	    	var Filter = function(val) {
	        	if(val == 'client')
	        		return 'Client';
	        	if(val == 'prospect')
	        		return 'Prospect';
	        	if(val == 'retracted')
	        		return 'Rétracté';
	        	return val;
	        }
	        return Filter;	
    	}        
    ]);
});
