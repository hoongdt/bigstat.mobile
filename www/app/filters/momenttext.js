define([
    'app'
], function(app) {
    'use strict';
    app.filter('momenttext', [
    	function () {
	    	var Filter = function(val, arg) {
	    		if(arg == "month")
	    			return val.format("MMMM");
	    		if(arg == "monthshort")
	    			return val.format("MMM");
	    		if(arg == 'week'){
	    			var nex7 = val.clone().add(7,'days');
	    			return val.date()+"-"+nex7.date()+' '+val.format("MMMM");
	    		}

	    		if(arg == 'day'){
	    			return val.format("dddd D MMMM");
	    		}

	    		if(arg == 'days'){
	    			return val.format("D MMMM YYYY");
	    		}

	    		if(arg == 'day2'){
	    			return val.format("D ")+ val.format("MMMM YYYY").capitalizeFirstLetter();
	    		}

	    		if(arg == 'hour1'){
	    			return val.format("H")+"H..";
	    		}

	    		if(arg == 'hour'){
	    			return val.format("dddd D MMMM H");
	    		}

	    		if(arg == 'hour2'){
	    			return val.format("H{0}mm").format('H');
	    		}
	    		if(arg == 'now'){
	    			var now = moment();
	    			var diff = now.diff(val, 'minutes');
	    			if(diff<10)
	    				return 'Now';
	    			diff = now.diff(val, 'days');
	    			if(diff>=1)
	    				return val.format("ddd");
	    			return val.format("H{0}mm").format('H');
	    		}

	        	return val;
	        }
	        return Filter;	
    	}        
    ]);
});
