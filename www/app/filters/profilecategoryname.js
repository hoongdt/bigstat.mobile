define([
    'app'
], function(app) {
    'use strict';
    app.filter('profilecategoryname', [
    	function () {
	    	var Filter = function(val) {
	        	if(val == 'prospector')
	        		return 'PROSPECTEUR';
	        	if(val == 'commercial')
	        		return 'COMMERCIAUX';
	        	if(val == 'serviceAgent')
	        		return 'AGENT DE SERVICE';
	        	if(val == 'pands')
	        		return 'P&S';
	        	return val;
	        }
	        return Filter;	
    	}        
    ]);
});
