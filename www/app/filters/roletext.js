define([
    'app'
], function(app) {
    'use strict';
    app.filter('roletext', [
    	function () {
	    	var Filter = function(val, arg) {
	    		if(val == 'prospector')
	    			return 'Prospecteur';
	    		if(val == 'commercial')
	    			return 'Commercial';
	    		if(val == 'director')
	    			return 'Directeur';
	    		if(val == 'agence')
	    			return 'Agence';
	    		if(val == 'service-agent')
	    			return 'Agent de service';
	    		return val;
	        }
	        return Filter;	
    	}        
    ]);
});