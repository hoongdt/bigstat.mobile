define([
    'app',
    'common/Messenger'
], function(app, messenger) {
    'use strict';
    app.controller('listFilterInput', [
        '$scope',        
        '$state',
        function($scope, $state) {           
            var vm = {                
                gotoFilter: function(){
                    $state.go('tab.{0}-list-filter'.format($scope.fortab),{view: $scope.forview});
                },                
                _waitTime: null,
                textchanged: function(){
                    var that = this;                    
                    this._waitTime = moment();
                    var sensitive = 500;
                    setTimeout(function(){
                        var now = moment();                        
                        var duration = now.diff(that._waitTime);                        
                        if(duration>=sensitive){
                            raise();
                            that._waitTime = now;
                        }
                    },sensitive);
                },
                text: null
            };            

            var criterias = null;

            function raise() {                
               if($scope.$parent.onFilter)
                    $scope.$parent.onFilter({
                        text: vm.text,
                        criterias: criterias
                    });  
            }


            
            messenger.register('listfilter.submit')
            .done(function(r){
                criterias = r;
                raise();
            });

            $scope.vm = vm;
        }
    ]);
});
