define([
    'app',
    'common/Messenger'
], function(app, messenger) {
    'use strict';
    app.controller('profilePicker', [
        '$scope',
        '$ionicHistory',     
        '$state',  
        function($scope, $ionicHistory, $state) {
            var vm = {
                category: $state.params.category,
                multiPicker: $state.params.multi,
                items: null,
                submit: function(){
                    messenger.raise("pick-profile", this.items
                        .where(function(it){
                            return it.checked;
                        }));
                    $ionicHistory.goBack();
                }
            };
            $scope.vm = vm;
            $scope.onSelect = function (r) {
                messenger.raise("pick-profile", r);
                $ionicHistory.goBack();
            };
        }
    ]);
});
