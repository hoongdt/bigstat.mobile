define([
    'app'
], function(app) {
    'use strict';

    app.controller('smallMap', [
        '$scope',
        function($scope) {

            var location = $scope.$parent.getLocation? 
            $scope.$parent.getLocation()
            :{
                latitude: 48.852063,
                longitude: 2.355841
            };
        	var vm = {
                map:{ center: location, zoom: 8 }
			};
			$scope.vm = vm;            
        }
    ]);

});
