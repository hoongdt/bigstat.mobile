define([
    'app','ioc!saleService'
], function(app, service) {
    'use strict';

    app.controller('saleList', [
        '$scope',
        '$state',
        function($scope, $state) {
        	var vm = {
				test:"HERO",
                _view:'month',
                items:[],
                cangoupper: function(){
                    return this.view()!='month';
                },
                upperview: function(){
                    if(this._view=='week')
                        return 'mois';
                    if(this._view=='day')
                        return 'semaine';                    
                    return 'Unknow up';
                },
                goup: function(){
                    if(this._view == 'day'){
                        this.view('week');
                    }
                    else if(this._view == 'week'){
                        this.view('month');
                    }                   
                },
                godown: function(){
                    if(this._view == 'month'){
                        this.view('week');
                    }
                    else if(this._view == 'week'){
                        this.view('day');
                    }                                      
                },
                view: function(val){
                    if(!val)
                        return this._view;
                    this._view = val;
                    this.retrieve();
                },
                retrieve: function(){
                    vm.items.length = 0;
                    if(this._view == 'month'){
                        service.getSaleByMonth().done(function (r) {
                            vm.items.addRange(r);
                        });
                    }
                    else if(this._view == 'week'){
                        service.getSaleByWeek().done(function (r) {
                            vm.items.addRange(r);
                        });
                    }
                    else if(this._view == 'day'){
                        service.getSaleByDay().done(function (r) {
                            vm.items.addRange(r);
                        });
                    }                   
                },
                viewSale: function(it){
                    $state.go('tab.sale-view', {id: it.id});
                }
			};
			$scope.vm = vm;
            vm.retrieve();
        }
    ]);

});
