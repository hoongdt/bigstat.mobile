define([
    'app'
], function(app) {
    'use strict';
    app.controller('dashboardStat', [
        '$scope',
        function($scope) {
        	var vm = {
        		_view: "month",
				chosen: function(v){
					return this._view == v;
				},
				goto: function(v){
					this._view = v;
				}
        	};

        	$scope.vm = vm;
        }
    ]);

});
