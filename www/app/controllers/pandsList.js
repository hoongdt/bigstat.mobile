define([
    'app',
    'ioc!pandsService'
], function(app, service) {
    'use strict';

    app.controller('pandsList', [
        '$scope',
        '$state',
        function($scope, $state) {
        	var vm = {
                items:[],
                pick: function(it){
                    if($scope.$parent.forpicker)
                        $scope.$parent.onSelect(it);
                }
			};
			$scope.vm = vm;
            service.getList().done(function(r){
                vm.items.addRange(r);
            });
        }
    ]);

});
