define([
    'app'
], function(app) {
    'use strict';
    app.controller('search', [
        '$scope',
        '$state',
        function($scope, $state) {
        	var vm = {
                _choose: $state.params.view,
                chosen: function(it) {
                    return it == this._choose;
                },
                goto: function(it){
                    $state.go('tab.search', {view: it});
                }
        	};

            $scope.vm = vm;
        }
    ]);

});
