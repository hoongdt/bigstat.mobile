define([
    'app'
], function(app) {
    'use strict';
    app.controller('pandsListFilter', [
        '$scope',
        '$ionicHistory',
        function($scope, $ionicHistory) {
            var backView  = $ionicHistory.backView();
            var vm = {
                submit: function() {
                    $ionicHistory.goBack();
                },
            };

            $.extend($scope, vm);
        }
    ]);
});
