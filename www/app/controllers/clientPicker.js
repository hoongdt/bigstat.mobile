define([
    'app',
    'common/Messenger'
], function(app, messenger) {
    'use strict';
    app.controller('clientPicker', [
        '$scope',
        '$ionicHistory',       
        function($scope, $ionicHistory) {
            $scope.onSelect = function (r) {
                messenger.raise("pick-client", r);
                $ionicHistory.goBack();
            };
        }
    ]);
});
