define([
    'app'
], function(app) {
    'use strict';

    app.controller('dashboard', [
        '$scope',
        function($scope) {
        	 var db = {
        	 	lists : [
				        {'id': 'PROSPECTEUR', 'label': 'PROSPECTEUR'},
				        {'id': 'COMMERCIAL', 'label': 'COMMERCIAL'},
				        {'id': 'MANAGER', 'label': 'MANAGER'},
				        {'id': 'CEO', 'label': 'CEO'},
				        {'id': 'SERVICE AGENT', 'label': 'SERVICE AGENT'},
    				],
    			dashboard_type: 'MANAGER',    			
        	 	// unitChanged : function(){
        	 	// 	 dashboard_type = dataselect.seletecItem.value;        	 		
        	 	// },
        	 	// update : function(){
        	 	// 	 dashboard_type = data.seletecItem.value;        	 		
        	 	// }
        	 } ;
        	 
        	var vm = {           
                companyName: 'UNION OUEST HABITAT',
                agencyName: 'LE MANS',
                phone: '02 43 86 99 06',
                image: 'app/dummy/image/union.jpg',
                imageprofile: 'app/dummy/image/profile.jpg',
                numberofRDV: 78,
                percent: 64,
                totalClass: 11,
                classTrend : 0,
                reliability : 33,
                reliabilityStatus : 1,
                rdvPerDay : 3.4,
                rdvPerDayStatus: -1,
                user: {
                    role: 'prospector'
                }
        	};

        	var vm_com ={
        		companyName: 'UNION OUEST HABITAT',
                agencyName: 'LE MANS',
                phone: '02 43 86 99 06',
                image: 'app/dummy/image/union.jpg',
                imageprofile: 'app/dummy/image/profile.jpg',
                numberofCA: 105,
                percent: 64,
                totalClass: 8,
                classTrend : 0,
                reliability : 38,
                reliabilityStatus : -1,
                rdvPerDay : 5.5,
                rdvPerDayStatus: 1,
        	}

        	var vm_man ={
        		companyName: 'UNION OUEST HABITAT',
                agencyName: 'LE MANS',
                agencyRank: 2,
                phone: '02 43 86 99 06',
                image: 'app/dummy/image/union.jpg',
                imageprofile: 'app/dummy/image/profile.jpg',
                numberofCA: 105,
                percent: 64,
                totalClass: 8,
                classTrend : 1,
                reliability : 67,
                reliabilityStatus : -1,
                rdvPerDay : 5.5,
                rdvPerDayStatus: 1,
        	}

            $scope.vm = vm;
            $scope.db = db;
            $scope.vm_com = vm_com;
            $scope.vm_man = vm_man;
        }
    ]);

});
