define([
    'app',
    'ioc!appointmentService'
], function(app, service) {
    'use strict';
    app.controller('sellerAppointment', [
        '$scope',
        '$state',
        function($scope, $state) {
            var vm = {
               appointment: null,
            };
            service.getAppointment($state.params.id)
            .done(function(r){
                vm.appointment = r;
            });
            $scope.vm = vm;
        }
    ]);
});
