define([
    'app',
    'common/Messenger'
], function(app, mesenger) {
    'use strict';
    app.controller('listFilter', [
        '$scope',
        '$ionicHistory',
        '$state',
        function($scope, $ionicHistory, $state) {
            var backView  = $ionicHistory.backView();

            var filterfor = $state.params.view;
            var multipleText = null;
            var multiple = [];
            var singleText = null;
            var single = [];
            var title = null;
            if(filterfor == "client-list"){                
                title = 'FILTRE CLIENT';
                multipleText = "FILTRER PAR";
                singleText = "DATE D’AJOUT";
                // multiple = ["prospect", "client","Retracté","CA"];
                multiple = ["prospect", "client","Retracté"];
                single = ["today", "thisweek", "thismonth", "nofilter"];
                //  check context here
            }
            else if(filterfor == "appointment-list"){
                title = 'FILTRE RDV';
                multipleText = "FILTRER PAR";
                singleText = "DATE";
                multiple = ["Annulé", "Absent","Reporté","Sans suite","Négociation","Vente"];
                single = ["Aujourd’hui", "Semaine", "Mois", "Indifférent"];
                //  check context here
            }
            else if(filterfor == "pands-list"){
                title = 'FILTRE P&S';
                multipleText = "FILTRER PAR";
                singleText = "DATE";
                multiple = ["CA tenu", "CA facturé","CA annulé","Quantité vendue","Négociation","Ordre alphabétique"];
                single = ["Aujourd’hui", "Cette semaine", "Ce mois ci", "Année", "Indifférent"];
                //  check context here
            }
            else if(filterfor == "sale-list"){
                title = 'FILTRE VENTE';
                multipleText = "FILTRER PAR";
                singleText = "DATE";
                multiple = ["Vente en attente", "Service prévu","Service en cours","Service accomplie","Facturée","Annulée", "Tout"];
                single = ["Aujourd’hui", "Cette semaine", "Mois", "Indifférent"];
                //  check context here
            }

            else if(filterfor == "profile"){
                title = 'FILTRE PROFIL';
                multipleText = "FILTRER PAR";
                singleText = "DATE";
                multiple = ["Rdv tenu", "Taux de fiabilite","CA","CA honore","CA réel","Taux de transformation","Taux d’annulation","Panier moyen", "P&S", "Quantité"];
                single = ["SEMAINE", "MOIS", "ANNEE", "TOUT"];
                //  check context here
            }

            multiple = multiple.select(function(it){
                return {
                    code: it,
                    checked: false
                }
            });

            single = single.select(function(it){
               return {
                    code: it,
                    checked: false
                } 
            });
            
            var vm = {
                submit: function() {
                    $ionicHistory.goBack();
                    mesenger.raise('listfilter.submit', {
                        multiple: vm.multiple.where(function(it){
                            return it.checked;
                        })
                        .select(function(it){
                            return it.code
                        }),
                        single: vm.single.where(function(it){
                            return it.code == vm.singleChoice;
                        })
                        .select(function(it){
                            return it.code
                        })
                    });
                },
                singleChoice: null,
                multiple: multiple,
                multipleText: multipleText,
                singleText: singleText,
                single: single,
                title: title
            };

            $scope.vm = vm;
        }
    ]);
});
