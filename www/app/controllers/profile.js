﻿define([
    'app',
    'ioc!profileService'
], function(app, service) {
    'use strict';
    app.controller('profile', [
        '$scope',    
        '$state',    
        function($scope, $state) {
            var vm = {     
                profile: null        
            };
            service.getProfile($state.params.id)
            .done(function(r){
                vm.profile = r;
            });

            $scope.vm = vm;
        }
    ]);
});
