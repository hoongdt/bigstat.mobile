define([
    'app',
    'ioc!clientService',
    'common/Messenger',
    'common/pickers'
], function(app,service, messenger, pickers) {
    'use strict';
    app.controller('clientCreate', [
        '$scope',
        '$ionicHistory',       
        function($scope, $ionicHistory) {
            var vm = {
                client: {
                    firstName: null,
                    lastName: null,
                    address: null,
                    telephone1: null,
                    telephone2: null,
                    mail: null,
                    type: "client",
                    address: {
                        text: null,
                        latitude: 48.852063,
                        longitude: 2.355841
                    },
                    product:{
                        name: null
                    }
                },
                submit: function() {
                    service.createClient(this.client)                    
                    .done(function(r){
                        $ionicHistory.goBack();
                        messenger.raise('create-client');
                    });
                },
                isChoosen: function(it) {
                    return this.client.type == it;
                },
                choose: function(it) {
                    this.client.type = it;
                },
                locationchanged: function(it){
                    var geocoder = new google.maps.Geocoder();
                    geocoder.geocode({ 'address': it }, function (results, status) {
                        if (status === google.maps.GeocoderStatus.OK) {  
                            var result = results[0];
                            vm.client.address.longitude = result.geometry.location.lng();
                            vm.client.address.latitude = result.geometry.location.lat();                            
                        } else {
                            console.trace('Geocode was not successful for the following reason: ' + status);
                        }                        
                    });
                },
                pickProduct: function(){
                    var that = this;
                    pickers.pickpands().done(function(r){
                        that.client.product = r;
                    });
                }
            };

            $scope.getLocation = function(){
                return vm.client.address;
            };            

            $scope.vm = vm;
        }
    ]);
});
