define([
    'app'
], function(app) {
    'use strict';
    app.controller('profileHeading', [
        '$scope',
        '$ionicHistory',
        function($scope, $ionicHistory) {
            var vm = {
                percent: 70,
                profile: {
                    firstName: 'Mathieu',
                    lastName: 'MORIN',
                    image: 'app/dummy/avatar.jpg'
                }
            };
            $scope.vm = vm;             
        }
    ]);
});
