define([
    'app',
    'ioc!userService'
], function(app, userService) {
    'use strict';
    app.controller('login', [
        '$scope',
        '$ionicHistory',
        '$ionicNavBarDelegate',
        '$state',
        function($scope, $ionicHistory, $ionicNavBarDelegate, $state) {
        	$ionicHistory.clearHistory();        	
            var vm = {
                login: function(){
                    userService.login(this.email, this.password)
                    .done(function(r){
                        if(r)
                            $state.go('tab.dashboard');
                    });
                },
                email: null,
                password: null
            };

            $scope.vm = vm;
        }
    ]);

});
