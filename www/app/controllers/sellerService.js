define([
    'app', 
    'ioc!saleService',
], function(app, service) {
    'use strict';
    app.controller('sellerService', [
        '$scope',
        '$ionicHistory',
        function($scope, $ionicHistory) {
            var vm = {
               items : null               
            };
            service.getSaleAsServices(1)
            .done(function(r){
                vm.items = r;
            });       
            $scope.vm = vm;
        }
    ]);
});
