define([
    'app',
    'common/Messenger'
], function(app, messenger) {
    'use strict';
    app.controller('pandsPicker', [
        '$scope',
        '$ionicHistory',       
        function($scope, $ionicHistory) {
            $scope.onSelect = function (r) {
                messenger.raise("pick-pands", r);
                $ionicHistory.goBack();
            };
        }
    ]);
});
