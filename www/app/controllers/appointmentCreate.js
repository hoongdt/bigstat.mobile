define([
    'app',
    'common/Messenger',
    'common/pickers',
    'ioc!appointmentService',
], function(app, messenger, pickers, service) {
    'use strict';
    app.controller('appointmentCreate', [
        '$scope',
        '$ionicHistory',
        '$rootScope',
        function($scope, $ionicHistory, $root) {
            var vm = {
                submit: function() {
                    service.addAppointment({
                        clientId: this.client.id,
                        date: moment(this.date).format(),
                        note: this.note,
                        userGuidSet: [this.commercial.select(function(it){
                            return it.id;
                        })]
                    })
                    .done(function(){
                        $ionicHistory.goBack();
                    });
                },
                isClient: true,
                isService: true,
                pands: null,
                client: null,
                prospector: null,
                commercial: null,
                commercials: function(){
                    if(!this.commercial)
                        return null;
                    return this.commercial.aggregate(function(a,b){
                        return !b?a.firstName: a.firstName + ", " + b.firstName;
                    });

                },
                date: new Date(),
                note: "le client a déjà de l’isolation, maison de 83 ans.",
                isChoosen: function(it) {
                    if (it == 'client' && this.isClient)
                        return true;
                    if (it == 'prospect' && !this.isClient)
                        return true;
                    return false;
                },
                choose: function(it) {
                    if (it == 'client'){
                        this.isClient = true;
                        return
                    }
                    if (it == 'prospect'){
                        this.isClient = false;
                        return
                    }
                },
                pick: function(what){
                    var that = this;
                    if(what == 'pands')
                        pickers.pickpands().done(function(r){
                            that.pands = r;
                        });
                    else if(what == 'client')
                        pickers.pickClient().done(function(r){
                            that.client = r;
                        });
                    else if(what == 'prospector')
                        pickers.pickProfile('prospector').done(function(r){
                            that.prospector = r;
                        });
                    else if(what == 'commercial')
                        pickers.pickProfile('commercial', true).done(function(r){
                            that.commercial = r;
                        });
                }
            };
            $scope.vm = vm;
        }
    ]);
});
