define([
    'app',
    'ioc!dashboardService'
], function(app, service) {
    'use strict';
    app.controller('dashboardStatDay', [
        '$scope',
        function($scope) {
        	var vm = {
        		data: {

                }
        	};

        	$scope.vm = vm;
            service._getDayStat().done();
        }
    ]);

});
