define([
    'app','ioc!appointmentService'
], function(app, service) {
    'use strict';

    app.controller('appointmentList', [
        '$scope',
        '$state',
        function($scope, $state) {
        	var vm = {
				test:"HERO",
                _view:'month',
                items:[],
                cangoupper: function(){
                    return this.view()!='month';
                },
                upperview: function(){
                    if(this._view=='week')
                        return 'mois';
                    if(this._view=='day')
                        return 'semaine';
                    if(this._view=='hour')
                        return 'jour';
                    return 'Unknow up';
                },
                goup: function(){
                    if(this._view == 'day'){
                        this.view('week', this._current.time.startOf('month'));
                    }
                    else if(this._view == 'week'){
                        this.view('month');
                    }
                    else if(this._view == 'hour'){
                        this.view('day', this._current.time.startOf('week'));
                    }
                },
                godown: function(options){
                    if(this._view == 'month'){
                        this.view('week', options);
                    }
                    else if(this._view == 'week'){
                        this.view('day', options);
                    } 
                    else if(this._view == 'day'){
                        this.view('hour', options);
                    }                    
                },
                _current: null,
                view: function(val, options){
                    if(!val)
                        return this._view;
                    this._current = options;
                    this._view = val;
                    this.retrieve(options);
                },
                retrieve: function(options){
                    vm.items.length = 0;
                    var time = options;
                    if(options && options.time)
                        time = options.time;
                    if(this._view == 'month'){
                        service.getAppointmentByMonth().done(function (r) {
                            vm.items.addRange(r);
                        });
                    }
                    else if(this._view == 'week'){
                        service.getAppointmentByWeek(time).done(function (r) {
                            vm.items.addRange(r);
                        });
                    }
                    else if(this._view == 'day'){
                        service.getAppointmentByDay(time).done(function (r) {
                            vm.items.addRange(r);
                        });
                    }
                    else if(this._view == 'hour'){
                        service.getAppointmentByHour(time).done(function (r) {
                            vm.items.addRange(r);
                        });
                    }
                },
                viewAppointment: function(it){
                    $state.go('tab.seller', {view: 'appointment', id:it.id});
                }
			};
			$scope.vm = vm;
            $scope.onFilter = function(filter){
                vm.retrieve();
            };
            vm.retrieve();
        }
    ]);

});
