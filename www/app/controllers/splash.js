define([
    'app',
    'ioc!userService'
], function(app, userService) {
    'use strict';
    app.controller('splash', [
        '$scope',
        '$state',
        '$ionicHistory',
        function($scope, $state, $ionicHistory) {
        	setTimeout(function(){
        		userService.authorized()
        		.done(function(r){        			
        			if(r)
        				$state.go('tab.dashboard');
        			else
        				$state.go('login');
        		});

        	}, 1000);        	
        }
    ]);

});
