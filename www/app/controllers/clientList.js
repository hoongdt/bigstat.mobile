define([
    'app',
    'ioc!clientService',
    'common/pickers'
], function(app, service, pickers) {
    'use strict';

    app.controller('clientList', [
        '$scope',
        function($scope) {
        	var vm = {
				items:[],
                pick: function(it){
                    if($scope.forpicker)
                        $scope.$parent.onSelect(it);
                },
                createClient: function(){
                    pickers.createClient().done(function (r) {
                       fetchdata(); 
                    });
                }
			};
			$scope.vm = vm;
            function fetchdata (filter) {
                service.searchClient(filter)
                .done(function(r){
                    vm.items.length = 0;
                    vm.items.addRange(r);
                });
            }
            fetchdata();
            $scope.onFilter = function(filter){
                fetchdata(filter);
            }	        
        }
    ]);

});
