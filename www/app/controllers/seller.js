define([
    'app'
], function(app) {
    'use strict';
    app.controller('seller', [
        '$scope',
        '$state',
        function($scope, $state) {
            var vm = {
                view: function(v){
                    return v==$state.params.view;
                },
                goto: function(v){
                    $state.go('tab.seller',{view: v});
                },
                canCreateSale: function(){
                    if(this.saleCreated)
                        return false;
                    // TODO: other condition
                    return true;
                },
                
                saleCreated: false,
                serviceCreated: false,
                canCreateService: function(){
                    if(this.serviceCreated || !this.saleCreated)
                        return false;
                    // TODO: other condition
                    return true;
                }
            };

            $scope.vm = vm;
        }
    ]);
});
