define([
    'app',
    'ioc!userService'
], function(app, userService) {
    'use strict';

    app.controller('tabs', [
        '$scope',
        '$ionicPopover',
        '$state',
        function($scope, $ionicPopover, $state) {
            var template = $('#more-template').html();
            var pop = $ionicPopover.fromTemplate(template, {
                scope: $scope
            });
            var vm = {
                popOverClick: function(e) {
                    pop.show(e);
                },
                logout: function(){
                    pop.hide();
                    userService.logoff();
                    $state.go('login');
                }
            };
            $scope.vm = vm;
        }
    ]);

});