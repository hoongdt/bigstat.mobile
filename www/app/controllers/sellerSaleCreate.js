define([
    'app', 
    'ioc!saleService',
    'common/Messenger',
    'common/pickers',
], function(app, service, messenger, pickers) {
    'use strict';
    app.controller('sellerSaleCreate', [
        '$scope',
        '$ionicHistory',
        '$ionicPopup',
        '$timeout',
        function($scope, $ionicHistory, $ionicPopup, $timeout) {
            var vm = {
                _saleStatus: 'new',  // status - new, pending, approve
                isAdditionalSale:false, 
                choseQuantity:null,       
                submit: function() {
                    if (this.isAdditionalSale){
                        this._saleStatus = 'additionalapprove';
                    }
                    else {
                        this._saleStatus = 'pending';
                    }
                    if ($scope.$parent){
                        $scope.$parent.vm.saleCreated = true;
                    }
                },
                edit: function() {
                    this._saleStatus = 'new';
                    if ($scope.$parent){
                        $scope.$parent.vm.saleCreated = false;
                    }
                },
                charge: function() {
                    if (!this.isAdditionalSale){
                        this._saleStatus = 'approve';
                    }
                },
                accept: function() {
                    if (!this.isAdditionalSale){
                        this._saleStatus = 'new';
                        this.isAdditionalSale = true;
                    }
                },

                addnewproduct: function(){
                    this.listProducts.add({productName : '',
                        quantity: null,
                        productPrice : null,
                        diver :'',
                        diverPrice:null});
                },

                pick : function(index) {
                    this.listProducts[index].productName = "asdasdd";                    
                    var that = this;
                    //console.log(that.listProducts[index].diverPrice);
                    pickers.pickpands()                   
                    .done(function(r){                                                 
                        that.listProducts[index].productName = r.name;
                        that.listProducts[index].productPrice = r.price;
                       // console.log(that.listProducts[index]);
                       
                        var promptPopup = $ionicPopup.prompt({
                             title: 'Input',
                             template: 'Input quantity',
                             inputType: 'number',
                             inputPlaceholder: 'quantity'
                          });
                            
                          promptPopup.then(function(res) {
                                that.listProducts[index].quantity = res;
                          });
                    });
                    
                },

                listProducts:[{
                    productName : '',
                    quantity: null,
                    productPrice : null,
                    diver :'',
                    diverPrice:null
                }],

                isCash: true,
                isFundingStatus:true,
                cashValue: 100,
                fundingValue: 100,                
                pendingSaleData : null,
                pendingAdditionalSaleData: null,

                isStatus: function(status){
                    if (status == this._saleStatus)
                        return true;
                    return false;
                },
                isChoosen: function(it) {
                    if (it == 'cash' && this.isCash)
                        return true;
                    if (it == 'funding' && !this.isCash)
                        return true;
                    if (it == 'fundingstatus' && this.isFundingStatus)
                        return true;
                    if (it == 'instudy' && !this.isFundingStatus)
                        return true;
                    return false
                },
                choose: function(it) {
                    if (it == 'cash'){
                        this.isCash = true;
                        return
                    }
                    else if (it == 'funding'){
                        this.isCash = false;
                        return
                    }
                    else if (it == 'fundingstatus'){
                        this.isFundingStatus = true;
                        return
                    }
                    else if (it == 'instudy'){
                        this.isFundingStatus = false;
                        return
                    }                 
                },                
            };
            service.getPendingSale(1)
            .done(function(r){
                vm.pendingSaleData = r;
            });
            service.getAdditionalPendingSale(1)
            .done(function(r){
                vm.pendingAdditionalSaleData = r;
            });
            $scope.vm = vm;
             
        }
    ]);
});
