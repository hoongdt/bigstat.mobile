define([],function(){
	if (!String.prototype.format) {
        String.prototype.format = function () {
            var args = arguments;
            return this.replace(/{(\d+)}/g, function (match, number) {
                return typeof args[number] != 'undefined'
                  ? args[number]
                  : match
                ;
            });
        };
    }

    if (!String.prototype.isNullOrEmpty) {
        String.prototype.isNullOrEmpty = function () {
            return !this || this == "";
        };
    }

    if (!String.prototype.replaceAll) {
        String.prototype.replaceAll = function (t, w) {
            var reg = new RegExp(t, 'g');
            return this.replace(reg, w);
        };
    }

    if (!String.prototype.capitalizeFirstLetter) {
        String.prototype.capitalizeFirstLetter = function() {
            return this.charAt(0).toUpperCase() + this.slice(1);
        }
    }

    

    if (!window.cloneObject)
        window.cloneObject = function (obj) {
            return JSON.parse(JSON.stringify(obj));
        }
    if (!window.__equals) {
        window.__equals = function(obj1, obj2) {
            return JSON.stringify(obj1) == JSON.stringify(obj2);
        };
    }

    function isEmpty(obj) {
        if (!obj)
            return true;
        if (obj.length == 0)
            return true;
        return false;
    }

    if (!window.isEmpty)
        window.isEmpty = isEmpty;

    function shortGuid() {
        return ("0000" + (Math.random() * Math.pow(36, 4) << 0).toString(36)).slice(-4);
    }

    if(!window.Stack)
        window.Stack = function Stack(array) {
            var items = array;
            this.pop = function(){
                var it = items[items.length-1];
                items.length = items.length-1;
                return it;
            };
            this.push = function(it){
                items.unshift(it);
            };
            this.top = function(){
                var it = items[0];                
                items.splice(0,1);
                return it;
            };
            this.length = function(){
                return items.length;
            }
        };


    if (!window.shortGuid)
        window.shortGuid = shortGuid;

    if(!window.setRole)
        window.setRole = function(role){
            var u = localStorage.getItem('user-info');
            if(!u)
                return false;
            if(u)
                u = JSON.parse(u);
            u.role = role;
            localStorage.setItem('user-info',JSON.stringify(u));
            return true;
        };

    if(!window.getUserInfo)
        window.getUserInfo = function(){
            var u = localStorage.getItem('user-info');
            if(!u)
                return false;
            return JSON.parse(u);   
        }
});