define({	
	filterTime: function(list, dateField, criteria){
		if(criteria == "today")
			return list.where(function(it){
				var d = it[dateField];
				var today = moment().format("YYYY MM DD");
				var check = moment(d).format("YYYY MM DD");
				return today == check;
			});
		if(criteria == "thismonth")
			return list.where(function(it){
				var d = it[dateField];
				var today = moment().format("YYYY MM");
				var check = moment(d).format("YYYY MM");
				return today == check;
			});
		if(criteria == "thisweek")
			return list.where(function(it){
				var d = it[dateField];
				var today = moment().week();
				var check = moment(d).week();
				return today == check;
			});

		return list;
	},
	filterMultiMatch: function(list, field, criterias){
		return list.where(function(it){
			var v = it[field];
			if(!criterias || criterias.length == 0)
				return true;
			if(criterias.indexOf(v)>=0)
				return true;
			return false;
		})
	}
});