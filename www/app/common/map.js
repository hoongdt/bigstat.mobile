define([], function(){
	return {
		mapRole: function(r){
			if(r == 'Technician')
				r = 'service-agent';
			else if(r == 'CallOperator')
				r = 'prospector';
			else if(r == 'Seller')
				r = 'commercial';
			else if(r =='Director')
				r = 'director';
			else if(r =='CEO')
				r = 'ceo';
			return r;
		}
	};
})