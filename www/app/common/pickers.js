define(["app", "./Messenger"], function(app, messenger){
	return {
		pickpands: function(){
            app.$state.go('tab.pands-picker');
            return messenger.register('pick-pands', true);
		},
		pickClient: function(){
			app.$state.go('tab.client-picker');
            return messenger.register('pick-client', true);
		},
		createClient: function(){
			app.$state.go('tab.client-create');
            return messenger.register('create-client', true);	
		},
		pickProfile: function(category, multi){
			app.$state.go('tab.profile-picker',{category: category, multi: multi});
            return messenger.register('pick-profile', true);
		}
	};
});