define([], function(){
	var Messenger = {
		_handlers:[],
		register: function(key, once){
			// var ret = $.Deferred();
			var ret = new MessengerCallback();
			this._handlers[key] = {
				defer: ret,
				once: once
			};
			return ret;
		},
		raise: function(key, message){
			if(!(key in this._handlers))
				return;
			var c = this._handlers[key];
			c.defer.resolve(message);
			if(c.once)
				delete this._handlers[key];
		}
	};

	var MessengerCallback = function(){
		var _handler = null;
		this.done = function(handler){
			_handler = handler;
		};
		this.resolve = function(data){
			_handler(data);
		}
	};

	return Messenger;
});