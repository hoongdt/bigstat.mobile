define({
	money: function(val, symbol){
		if(val<1000)
			return val+(symbol?symbol:'');
		return Math.round(val/1000)+"k"+(symbol?symbol:'');
	}
});