define(function(){
	if(!window.notImplementedException){
		window.notImplementedException = function(msg){
			throw new Error(msg?msg:'Not implemented exception!');
		};
	}

	if(!window.notSupportedException){
		window.notSupportedException = function(msg){
			throw new Error(msg?msg:'Not supported exception!');
		};
	}
});