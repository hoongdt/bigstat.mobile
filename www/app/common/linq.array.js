﻿define(function () {
    if (!Array.prototype.clear) {
        Array.prototype.clear = function () {
            this.splice(0, this.length);
        };
    }

    if (!Array.prototype.remove) {
        Array.prototype.remove = function (a) {
            var idx = this.indexOf(a);
            if (idx < 0)
                throw "Array doesn't contain item";
            this.splice(idx, 1);
        };
    }

    if (!Array.prototype.removeAt) {
        Array.prototype.removeAt = function (idx) {
            if (idx >= this.length)
                throw "Index is bigger than length";
            this.splice(idx, 1);
        };
    }

    if (!Array.prototype.add) {
        Array.prototype.add = function (a) {
            this.push(a);
        };
    }

    if (!Array.prototype.removeAt) {
        Array.prototype.removeAt = function (idx) {
            if (idx >= this.length)
                throw "Index is bigger than length";
            this.splice(idx, 1);
            return this;
        };
    }

    if (!Array.prototype.clone) {
        Array.prototype.clone = function () {
            return this.slice();
        };
    }

    if (!Array.prototype.where) {
        Array.prototype.where = function (fnc) {
            var op = [];
            for (var i = 0; i < this.length; i++) {
                var ii = this[i];
                if (fnc(ii))
                    op.push(ii);
            }
            return op;
        };
    }

    if (!Array.prototype.firstOrNull) {
        Array.prototype.firstOrNull = function (fnc) {
            for (var i = 0; i < this.length; i++) {
                var ii = this[i];
                if (fnc(ii))
                    return ii;
            }
            return null;
        };
    }

    if (!Array.prototype.exists) {
        Array.prototype.exists = function (fnc) {
            if (typeof fnc != "function") {
                return this.indexOf(fnc) >= 0;
            }
            for (var i = 0; i < this.length; i++) {
                var ii = this[i];
                if (fnc(ii))
                    return true;
            }
            return false;
        };
    }


    if (!Array.prototype.last) {
        Array.prototype.last = function () {
            return this[this.length - 1];
        };
    }

    if (!Array.prototype.selectMany) {
        Array.prototype.selectMany = function (fnc) {
            var op = [];
            for (var i = 0; i < this.length; i++) {
                var ii = fnc(this[i]);
                op = op.concat(ii);
            }
            return op;
        };
    }

    if (!Array.prototype.select) {
        Array.prototype.select = function (fnc) {
            var op = [];
            for (var i = 0; i < this.length; i++) {
                var ii = fnc(this[i], i);
                op.push(ii);
            }
            return op;
        };
    }


    if (!Array.prototype.first) {
        Array.prototype.first = function (selector) {
            var r = this.firstOrNull(selector);
            if (!r)
                throw "Array doesn't contain element";

            return r;
        };
    }


    if (!Array.prototype.insert) {
        Array.prototype.insert = function (item, at) {
            this.splice(at, 0, item);
        };
    }

    if (!Array.prototype.for) {
        Array.prototype.for = function (fnc) {
            for (var i = 0; i < this.length; i++) {
                fnc(this[i], i);
            }
        };
    }

    if (!Array.prototype.selectFirst) {
        Array.prototype.selectFirst = function (selector) {
            var r = this.firstOrNull(selector);
            if (!r)
                throw "Array doesn't contain element";

            return r;
        };
    }

    if (!Array.prototype.removeItems) {
        Array.prototype.removeItems = function (options) {
            if (typeof options == "function") {
                var fnc = options;
                options = this.where(function (it) {
                    return fnc(it);
                });
            }
            for (var i = 0; i < options.length; i++) {
                var idx = this.indexOf(options[i]);
                this.removeAt(idx);
            }
        };
    }

    if (!Array.prototype.max) {
        Array.prototype.max = function (fnc) {
            var ret = this[0];
            var maxval = fnc(ret);
            for (var i = 1; i < this.length; i++) {
                var it = this[i];
                var c = fnc(it);
                if (c > maxval) {
                    ret = it;
                    maxval = c;
                }
            }

            return ret;
        };
    }

    if (!Array.prototype.sum) {
        Array.prototype.sum = function (fnc) {
            var val = 0;
            for (var i = 0; i < this.length; i++) {
                var it = this[i];
                val+=fnc(it);
            }

            return val;
        };
    }

    if (!Array.prototype.single) {
        Array.prototype.single = function (fnc) {
            var ret = null;
            for (var i = 0; i < this.length; i++) {
                var it = this[i];
                var ok = fnc(it);
                if (ok && ret)
                    throw 'Array contains more';
                if (ok) {
                    ret = it;
                }
            }

            if (!ret)
                throw 'Array does not contains item';
            return ret;
        };
    }

    if (!Array.prototype.addRange) {
        Array.prototype.addRange = function (items) {
            for (var i = 0; i < items.length; i++) {
                this.push(items[i])
            };
        };
    }

    if (!Array.prototype.loop) {
        Array.prototype.loop = function (fnc) {
            for (var i = 0; i < this.length; i++) {
                fnc(this[i],i);
            }
        };
    }

    if (!Array.prototype.getIndex) {
        Array.prototype.getIndex = function (selector) {
            var it = this.firstOrNull(selector);
            if (!it)
                return null;
            return this.indexOf(it);
        };
    }

    if (!Array.prototype.travel) {
        Array.prototype.travel = function (childrenField, itemFnc) {
            for (var i = 0; i < this.length; i++) {
                var it = this[i];
                itemFnc(it);
                var children = it[childrenField];
                if (children)
                    children.travel(childrenField, itemFnc);
            }
        };
    }

    if (!Array.prototype.travelFind) {
        Array.prototype.travelFind = function (childrenField, findFnc) {
            for (var i = 0; i < this.length; i++) {
                var it = this[i];
                if (findFnc(it))
                    return it;
                var children = it[childrenField];
                if (children){
                    var f= children.travelFind(childrenField, findFnc);
                    if(f)
                        return f;
                }
            }
        };
    }

    if (!Array.prototype.increase) {
        Array.prototype.increase = function (fnc) {
            var ret = [];
            for (var i = this[0]; i < this[this.length-1]; i++) {
                ret.add(fnc(i));
            };

            return ret;
        };
    }

    if (!Array.prototype.random) {
        Array.prototype.random = function (fnc) {
            if(this.length == 2 && typeof this[0] == "number")
                return Math.floor(Math.random() * (this[1]-this[0]))+this[0];
            var idx =Math.floor(Math.random() * this.length);
            return this[idx];
        };
    }

    if (!Array.prototype.aggregate) {
        Array.prototype.aggregate = function (fnc) {
            if(this.length == 0)
                return null;
            if(this.length == 1)
                return fnc(this[0], null);
            var v = this[0];
            for (var i = 1; i < this.length; i++) {
                v = fnc(v,this[i])
            }

            return v;
        };
    }

    if (!Array.prototype.flattern) {
        Array.prototype.flattern = function (childrenField) {
            var ret = [];
            for (var i = 0; i < this.length; i++) {
                var it = this[i];
                ret.push(it);
                var children = it[childrenField];
                if (children)
                    ret.push(children.flattern(childrenField));
            }

            return ret;
        };
    }


});