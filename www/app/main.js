var require = {
  baseUrl: 'app',
  paths: {
    'ionic': '../lib/ionic/js/ionic.bundle',
    'jquery': '../lib/jquery/jquery-1.12.0.min',
    'gmapapi': 'http://maps.googleapis.com/maps/api/js?libraries=places,geometry&sensor=false',
    'gmap': '../lib/angular-google-maps/dist/angular-google-maps',
    'slogger': '../lib/angular-simple-logger/dist/angular-simple-logger',
    'lodash': '../lib/lodash/dist/lodash',
    'moment': '../lib/moment/min/moment-with-locales.min',    
    'ion-place-autocomplete': '../lib/ion-place-autocomplete/ion-place-autocomplete',
    'ioc': 'services/ioc',
    'angular-chart': '../lib/angular-chart.js/dist/angular-chart',    
    'chart': '../lib/Chart.js/Chart'
  },  
  shim: {
    'ionic': {deps: ['jquery']},
    'slogger': {deps: ['ionic']},    
    'ion-place-autocomplete':{deps: ['gmapapi', 'ionic','gmap']},
    'gmap': {deps: ['gmapapi','ionic', 'slogger', 'lodash']},
    'angular-chart': {deps: ['ionic', 'chart']}
  },  
  priority: [
    'jquery',
    'ionic'
  ]
};
